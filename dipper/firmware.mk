# Firmware
PRODUCT_COPY_FILES += \
    vendor/xiaomi/firmware/dipper/abl.elf:install/firmware-update/abl.elf \
    vendor/xiaomi/firmware/dipper/aop.img:install/firmware-update/aop.img \
    vendor/xiaomi/firmware/dipper/bluetooth.img:install/firmware-update/bluetooth.img \
    vendor/xiaomi/firmware/dipper/cmnlib.img:install/firmware-update/cmnlib.img \
    vendor/xiaomi/firmware/dipper/cmnlib64.img:install/firmware-update/cmnlib64.img \
    vendor/xiaomi/firmware/dipper/devcfg.img:install/firmware-update/devcfg.img \
    vendor/xiaomi/firmware/dipper/dsp.img:install/firmware-update/dsp.img \
    vendor/xiaomi/firmware/dipper/hyp.img:install/firmware-update/hyp.img \
    vendor/xiaomi/firmware/dipper/keymaster.img:install/firmware-update/keymaster.img \
    vendor/xiaomi/firmware/dipper/logo.img:install/firmware-update/logo.img \
    vendor/xiaomi/firmware/dipper/modem.img:install/firmware-update/modem.img \
    vendor/xiaomi/firmware/dipper/qupfw.img:install/firmware-update/qupfw.img \
    vendor/xiaomi/firmware/dipper/storsec.img:install/firmware-update/storsec.img \
    vendor/xiaomi/firmware/dipper/tz.img:install/firmware-update/tz.img \
    vendor/xiaomi/firmware/dipper/xbl_config.img:install/firmware-update/xbl_config.img \
    vendor/xiaomi/firmware/dipper/xbl.img:install/firmware-update/xbl.img

# Firmware
PRODUCT_COPY_FILES += \
    vendor/xiaomi/firmware/sagit/abl.elf:firmware-update/abl.elf \
    vendor/xiaomi/firmware/sagit/BTFM.bin:firmware-update/BTFM.bin \
    vendor/xiaomi/firmware/sagit/cmnlib64.mbn:firmware-update/cmnlib64.mbn \
    vendor/xiaomi/firmware/sagit/cmnlib.mbn:firmware-update/cmnlib.mbn \
    vendor/xiaomi/firmware/sagit/devcfg.mbn:firmware-update/devcfg.mbn \
    vendor/xiaomi/firmware/sagit/hyp.mbn:firmware-update/hyp.mbn \
    vendor/xiaomi/firmware/sagit/keymaster.mbn:firmware-update/keymaster.mbn \
    vendor/xiaomi/firmware/sagit/logfs_ufs_8mb.bin:firmware-update/logfs_ufs_8mb.bin \
    vendor/xiaomi/firmware/sagit/NON-HLOS.bin:firmware-update/NON-HLOS.bin \
    vendor/xiaomi/firmware/sagit/rpm.mbn:firmware-update/rpm.mbn \
    vendor/xiaomi/firmware/sagit/storsec.mbn:firmware-update/storsec.mbn \
    vendor/xiaomi/firmware/sagit/tz.mbn:firmware-update/tz.mbn \
    vendor/xiaomi/firmware/sagit/xbl.elf:firmware-update/xbl.elf
